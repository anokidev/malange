"""

    malange.templates.cond

    This is the [cond] directive template.

    Copyright (c) 2024, All right reserved.
    Malange is licensed in MIT License.

"""

from typing import Sequence
from malange.types import MalangeCpm, MalangeVar, MalangeHTML

from .base_directive import BaseDirectiveTemplate, DirectiveTemplate

class CondDirectiveTemplate(BaseDirectiveTemplate):
    """The cond template directive."""

    name = "cond"
    rules = {
        'long'             : True,
        'short'            : True,
        'allow_direc_nest' : True,
        'can_return_none'  : True
    }

    long_attr     = ["$expression:pydata/keyword"]
    short_attr    = []

    long_content  = ["$HTML_Element:html", "!longelif^*", "!longelse*"]
    short_content = ["!shortcond&"]

    include       = {
        'shortcond' : ["$expression:pydata/keyword", r"%\:%", "$HTML_Element:html"],
        'longelif'  : ["?elif", "$HTML_Element:html"],
        'longelse'  : ["?else", "$HTML_Element:html"]
    }
    subdirec      = {
        'elif'      : ["$expression:pydata"],
        'else'      : []
    }


    @staticmethod
    def handle_long(handler) -> MalangeHTML | None:
        """This is the long direc handler."""

        # Get the if expression, stored in the long_attr.
        if_exp: MalangeVar  = handler.attr.get_var("expression")

        # Create a dict to hold elif.
        elif_tree: dict[MalangeVar, MalangeVar] | None = {}

        # Get the exp and the html of each elif. Set the exp as the key and the html
        # as the value. But if the variable is None, set the elif_tree accordingly.
        try:
            for i in handler.content.get_item(1):
                elif_tree[i.get_item(0).get_var("expression")] = i.get_var("HTML_Element")
        except AttributeError:
            elif_tree = None

        # Check if the if exp returns True
        if if_exp():
            return handler.content.get_var("HTML_Element")

        # Same with elif_tree. But since elif is a dict,
        # the first elif to return True will be selected.
        if elif_tree:
            for exp, html in elif_tree.items():
                if exp():
                    return html()

        # When everything else fails, return else.
        try:
            return handler.content.get_item(2).get_var("HTML_Element")()
        except AttributeError:
            return None

    @staticmethod
    def handle_short(handler) -> MalangeHTML | None:
        """Handle short direc."""

        statement: Sequence[MalangeCpm] = handler.content.get_item(0)
        tree: dict[MalangeVar, MalangeHTML] | None = {}

        for i in statement:
            tree[i.get_var("expression")] = i.get_var("HTML_Element")

class CondDirective(DirectiveTemplate):
    """This is the default cond directive."""

    def __init__(self):
        super().__init__(CondDirectiveTemplate)
