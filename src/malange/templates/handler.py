"""

    malange.template.handler

    Do you know the handler class that is
    passed to the handle_long and handle_short?
    This. This contains all you need to process shit.

"""

class LoadList:
    """This is the handler for attributes."""

    def __init__(self, data: list[str], include: dict[str, list[str]],
                 subdirec: dict[str, list[str]], list_type: str):
        self.list      = data
        self.include   = include
        self.subdirec  = subdirec
        self.list_type = list_type

        self.var_dict: dict[str, str]       = {}
        self.var_list: dict[int, str]       = {}
        self.cpm_list: dict[str, LoadList]  = {}
        self.sd_list:  dict[str, LoadList]  = {}
        self.keyword_list: dict[int, str]   = {}

        self.load_list()

    def load_list(self):
        """This will load the data and parse it."""

        for index, item in enumerate(self.list):
            if item[0] == "$":

                # First of all, we need to know if the variable is optional.
                optional: bool = bool(item[-1] == "*")

                # Get the type.
                if ":" in item:

                    # Yes I know that there is a possiblity
                    # that the dev might put multiple :, no
                    # worry I pick the first item.

                    try:
                        i: int = item.index(":")
                    except ValueError:
                        raise

                    # Get the variable type.
                    var_name = item[:i]

                    if optional:
                        var_type = item[i+1:-1]
                    else:
                        var_type = item[i+1:]

                    # If var name is empty:
                    if not var_name:
                        raise

                    # If var_type is empty:
                    if not var_type:
                        raise

                    # Check the type:
                    if var_type == "pydata":
                        self.var_dict[var_name] = var_type
                        self.var_list[index]    = var_name
                    elif var_type == "keyword":
                        self.var_dict[var_name] = var_type
                        self.var_list[index]    = var_name
                    elif var_type == "html":
                        self.var_dict[var_name] = var_type
                        self.var_list[index]    = var_name
                    else:
                        raise 
                else:
                    raise

            elif item[0] == "!":

                # The last two char.
                char: tuple[str, str] = (item[-2], item[-1])

                optional = bool("*" in char)
                multiple = bool("^" in char)

                # i is a included component
                cpm_name: str = item[1:]
                loaded_cpm = LoadList(self.include[cpm_name], self.include,
                                      self.subdirec, "component")
                self.cpm_list[cpm_name] = loaded_cpm

            elif item[0] == "?":
                # using subdirec is invalid.
                # throw error.
                if not self.subdirec:
                    raise

                subdirec_name: str          = item[1:]
                loaded_sd = LoadList(self.include[subdirec_name],
                                     self.include, self.subdirec, "component")
                self.sd_list[subdirec_name] = loaded_sd
            else:
                # i is a keyword.
                self.keyword_list[index] = item

    def get_item(self, index: int):
        """get_item method."""

        returned_item = None

        def try_list(the_list, index: int):
            try:
                return the_list[index]
            except KeyError:
                return None

        for i in (self.var_list, self.sd_list, self.cpm_list):
            item = try_list(i, index)
            if item is None:
                continue
            returned_item = item
            break

        if returned_item is None:
            raise

        return returned_item

    def get_var(self, var: str):
        """get_var method."""

        try:
            return self.var_dict[var]
        except KeyError:
            raise

class Content:
    """This is the handler for content."""

    def __init__(self, content, include, subdirec):
        self.content = content
        self.include = include
        if not subdirec:
            self.short = True
            self.subdirec = subdirec
        else:
            self.short = False
            self.subdirec = subdirec

class Handle:
    """This is the handler."""

    def __init__(self, attr: list[str], content: list[str],
                 include: dict[str, list[str]],
                 subdirec: dict[str, list[str]]):
        self.attr_data     = attr
        self.content_data  = content
        self.include       = include
        self.subdirec      = subdirec
        self.load()

    def load(self):
        """Load the attr and content class."""
        self.attr_c    = LoadList(self.attr_data,
                                  self.include, self.subdirec, "attr")
        self.content_c = Content(self.content_data,
                                 self.include, self.subdirec)

    @property
    def attr(self):
        """Attr handler."""
        return self.attr_c

    @property
    def content(self):
        """Content handler."""
        return self.content_c
