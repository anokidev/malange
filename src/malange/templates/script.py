"""

    malange.templates.script

    This is the [script] directive template.

    Copyright (c) 2024, All right reserved.
    Malange is licensed in MIT License.

"""

from .base_directive import BaseDirectiveTemplate, DirectiveTemplate

class ScriptDirectiveTemplate(BaseDirectiveTemplate):
    """The script template directive."""

    name = "script"
    rules = {
        'long'             : True,
        'short'            : True,
        'allow_direc_nest' : False,
        'can_return_none'  : False
    }

    long_attr     = []
    short_attr    = []

    long_content  = ["!script~"]
    short_content = ["!script"]

    include       = {
        'script' : ["$expression:text"]
    }
    subdirec      = {}

    @staticmethod
    def handles_long(handler):
        """This is the long handler."""

        expression: list[str] | None = []
        for i in handler.content.get_item("script"):
            expression.append(i.get_var("expression")())

        return handler.load_script(expression)

    @staticmethod
    def handles_short(handler):
        """This is the short handler."""

        return handler.load_script(
            handler.content.get_item("script").get_var("expression"))

class ScriptDirective(DirectiveTemplate):
    """This is the default script directive."""

    def __init__(self):
        super().__init__(ScriptDirectiveTemplate)
