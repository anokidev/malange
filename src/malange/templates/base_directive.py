"""

    malange.templates.base_directive

    This is the base directive template.

    Copyright (c) 2024, All right reserved.
    Malange is licensed in MIT License.

"""

from abc import ABC

from .handler import Handle

class BaseDirectiveTemplate(ABC):
    """This is the base."""

    name:          str
    rules:         dict[str, bool | None] = {
        'long'             : None,
        'short'            : None,
        'allow_direc_nest' : None,
        'can_return_none'  : None
    }

    long_attr:     list[str]
    short_attr:    list[str]
    long_content:  list[str]
    short_content: list[str]
    include:       dict[str, list[str]]
    subdirec:      dict[str, list[str]]

    @staticmethod
    def handle_short(handler):
        """The handler for short direc."""

    @staticmethod
    def handle_long(handler):
        """The handler for long direc."""

class DirectiveTemplate:
    """This is the template for direc."""

    def __init__(self, direc):
        self.name          = direc.name
        self.rules         = direc.rules
        self.long_attr     = direc.long_attr
        self.short_attr    = direc.long_attr
        self.long_content  = direc.long_content
        self.short_content = direc.short_content
        self.include       = direc.include
        self.subdirec      = direc.subdirec
        self.handle_short  = direc.handle_short
        self.handle_long   = direc.handle_long

        # Get the handle class.
        self.handle = self.load()

    def give_handle(self):
        """Give the handler, only after data has been loaded!"""

        short_data = self.handle_short(self.handle)
        long_data  = self.handle_long(self.handle)

    def load(self):
        """Load data."""

        if self.rules['short']:
            return Handle(attr=self.short_attr, content=self.short_content,
                   include=self.include, subdirec={})

        return Handle(attr=self.long_attr, content=self.long_content,
                include=self.include, subdirec=self.subdirec)
