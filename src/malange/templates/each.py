"""

    malange.templates.each

    This is the [each] directive template.

    Copyright (c) 2024, All right reserved.
    Malange is licensed in MIT License.

"""

from malange.types import MalangeVar, MalangeHTML

from .base_directive import BaseDirectiveTemplate, DirectiveTemplate

class EachDirectiveTemplate(BaseDirectiveTemplate):
    """The each template directive."""

    name    = "each"
    rules   = {
        'long'             : True,
        'short'            : True,
        'allow_direc_nest' : True,
        'can_return_none'  : False
    }

    long_attr     = ["$item:pycont", "in", "$list:pydata"]
    short_attr    = ["$item:pycont", "in", "$list:pydata"]

    long_content  = ["$HTML_Element:html"]
    short_content = ["$HTML_Element:html"]

    include       = {}
    subdirec      = {}

    @staticmethod
    def handle_long(handler) -> MalangeHTML:
        """This will handle the long direc."""

        cont: MalangeVar = handler.attr.get_var("item")

        iterable: MalangeVar = handler.attr.get_var("list")
        content:  MalangeVar = handler.content.get_var("HTML_Element")

        html_template: MalangeHTML = content()

        returned_html: MalangeHTML = handler.tool.create_html()

        for item in iterable():
            returned_html.add(html_template.inject(
                cont.fill(item).convert_to_dict()
            ))

        return returned_html


    @staticmethod
    def handle_short(handler):
        EachDirectiveTemplate.handle_long(handler)

class EachDirective(DirectiveTemplate):
    """This is the default each directive."""

    def __init__(self):
        super().__init__(EachDirectiveTemplate)
