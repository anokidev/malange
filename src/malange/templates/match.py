"""

    malange.templates.match

    This is the [match] directive template.

    Copyright (c) 2024, All right reserved.
    Malange is licensed in MIT License.

"""

from malange.types import MalangeVar, MalangeCpm, MalangeHTML

from .base_directive import BaseDirectiveTemplate, DirectiveTemplate

class MatchDirectiveTemplate(BaseDirectiveTemplate):
    """The each template directive."""

    name = "match"
    rules = {
        'long'             : True,
        'short'            : True,
        'allow_direc_nest' : True,
        'can_return_none'  : True
    }

    long_attr     = ["$item:pydata"]
    short_attr    = ["$item:pydata"]

    long_content  = ["!lmatch^"]
    short_content = ["!smatch&"]

    include       = {
        'lmatch' : ["?case^", "$HTML_Element:html"],
        'smatch' : ["$case:pydata/keyword", r"%\:%", "$HTML_Element:html"]
    }
    subdirec      = {
        'case'   : ['case', '$case:pydata/kword']
    }

    @staticmethod
    def handle_long(handler) -> MalangeHTML | None:
        """This will handle the long direc."""

        # This is the variable that is about to be matched.
        item: MalangeVar             = handler.attr.get_var("item")
        # This is the content of the direc.
        statement: tuple[MalangeCpm] = handler.content.get_item(0)

        # Temp dict to hold valid case.
        tree: dict[MalangeVar, MalangeVar] = {}
        # Previous case, I will explain later.
        previous_case: MalangeVar = handler.tool.create_var(None, "pydata", None)

        # We are going to loop in a for loop to check whether a case
        # is valid or not. This is done by checking whether there is
        # a duplicate or not. This is why previous_case is created.
        for i in statement:

            # This is the case subdirec.
            case: MalangeVar = i.get_item(0).get_var("case")
            # This is the html content.
            html: MalangeVar = i.get_var("HTML_Element")

            # If there is a duplicate, throw an error.
            if case.value() == previous_case():
                # throw error here
                pass
            # If not, add to the tree.
            else:
                tree[case]    = html
                previous_case = case

        # This is the html that is going to be rendered.
        rendered_html: MalangeHTML | None = None
        # This is the html that is going to be rendered.
        default_html: MalangeHTML | None  = None

        # Loop to check each case.
        for case, html in tree.items():
            # If the case is a Python data.
            if case.type() == "pydata":
                # If the data is the same as the data of the item.
                if case() == item():
                    rendered_html = html
            # If the case is a default.
            elif case.type() == "keyword":
                if case() == "default":
                    default_html = html

        if rendered_html is None:
            return default_html
        else:
            return rendered_html

    @staticmethod
    def handle_short(handler) -> MalangeHTML | None:
        """This is for short rendering."""

        # This is the variable that is about to be matched.
        item: MalangeVar             = handler.attr.get_var("item")
        # This is the content of the direc.
        statement: tuple[MalangeCpm] = handler.content.get_item(0)

        # Tree.
        tree: dict[MalangeVar, MalangeVar] = {}
        # Previous case.
        previous_case: MalangeVar = handler.tool.create_var(None, "pydata", None)

        # Do the same thing.
        for i in statement:

            # This is the case subdirec.
            case: MalangeVar = i.get_var("case")
            # This is the html content.
            html: MalangeVar = i.get_var("HTML_Element")

            # If there is a duplicate, throw an error.
            if case() == previous_case():
                # throw error here
                pass
            # If not, add to the tree.
            else:
                tree[case]    = html
                previous_case = case

        # This is the html that is going to be rendered.
        rendered_html: MalangeHTML | None = None
        # This is the html that is going to be rendered.
        default_html: MalangeHTML | None  = None

        # Loop to check each case.
        for case, html in tree.items():
            # If the case is a Python data.
            if case.type() == "pydata":
                # If the data is the same as the data of the item.
                if case() == item():
                    rendered_html = html
            # If the case is a default.
            elif case.type() == "kword":
                if case() == "default":
                    default_html = html

        if rendered_html is None:
            return default_html

        return rendered_html

class MatchDirective(DirectiveTemplate):
    """This is the default each directive."""

    def __init__(self):
        super().__init__(MatchDirectiveTemplate)
