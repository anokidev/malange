"""

    malange.scanner.main

    This is the main class of the scanner component.

    Copyright (c) 2024, All right reserved.
    This software is licensed in MIT License.

"""

class LexicalAnalyze:

    def __init__(self, raw_text: str) -> None:
        self.__raw_text: str = raw_text
        self.__begin_analyzing()

    def __begin_analyzing(self) -> None:
        """Begin analyzing."""

        self.__token_list: dict[int, str] = {}

        # Filter empty lines, then create a dictionary with key = line_num, value = line_content
        for index, content in enumerate(self.__raw_text.splitlines()):
            if content:
                self.__token_list[index] = self.__tokenizer(content)

    def __tokenizer(self, line: str):
        """Convert the line into token."""







