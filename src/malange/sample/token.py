"""

    malange.scanner.token

    This is the token class.

    Copyright (c) 2024, All right reserved.
    This software is licensed in MIT License.

"""

class TokenClass:
    """This is the main token class."""

    def __init__(self, token: str, position: int):
        self.char: str         = token
        self.position: int     = position


    def __call__(self):
        pass

    def _(self) -> None:
       """"""

    def __str__(self) -> str:
        return ""
