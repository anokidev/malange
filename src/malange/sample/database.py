"""

    malange.scanner.database

    This is the database of all tokens.

    HOW TO READ:
    - Each directive is case sensitive, and must be a dictionary.
    - The typical naming scheme for directives is lower snake case.
    - The content of directives are key_group (key) and keys (value)

    DIRECTIVE = {
        KEY_GROUP : {
            ELEMENT : [ITEM1, ITEM2, ITEM3]
        }
        KEY_GROUP : {ITEM1, ITEM2, ITEM3}
    }

    THE USAGE OF THE KEYS:
    - A directive contains key groups:
        - long_attr  = Used as attributes for long directives. Type list[str]
        - short_attr = Used as attributes for short directives. Type list[str]
        - block      = Used for the content of long directives. Type list.
        - subdirec   = Used for subdirectives. Type dir[str, list[str]]
        - include    = Used for includible elements. Type dir[str, list[str]]
    - long_attr key group is used to define attributes for long directives.
        - The type is a list of items. subdirec items are not allowed.
        - If the long_attr is like this: ['test', '=', '$content'], and the name
          of the direc is "test-direc" ...
          [test-direc test="abc"] ---> $content == "abc"
    - shor_attr key group is the same as long_attr, but this time for short directives.
        - The type is a list of items. subdirec elements are not allowed.
        - Assuming that the items inside short_attr is the same as the example in the
          long_attr:
          [test-direc :: test="abc"/]
    - block key group is a list containing elements for the content of long directives.
        - The elements can be subdirec (labeled by :), and includible elements (labeled by @)
        - Order matters!
    - subdirec is used to define subdirective elements.
    - include is used to define includible elements.

    SPECIAL CHARACTERS:
    - A special character can be used to add special characteristics.
    - : is used to label subdirective ITEMS in block.
        - Can't be used for items of long_attr and short_attr.
        - Used to mark ITEMS, not ELEMENTS.
        'subdirec'  =  {
            'example' : ['test', 'abc'] # This is a subdirective element.
        }
        block = [':example', '$HTML_Element'] # This is where : is used, in the subdirective item.
    - @ is similar to :, but for includible elements.
    - $ is used to mark items that are variables. Such as the $HTML_Element above.
        - Use it for items!
        - If the developer types "x < 10", that variable will contains str "x < 10"
    - % means space. Such as: [ 'test', '%', 'a'] means "test a", the space must exist.
        - Use it for items!
        - Must be used standalone!
        - It will be automatically inserted around a variable.
        - If the variable is at the end of the list, only before the variable space is inserted.
        - If the variable is at the beginning, only after the variable space is inserted.
    - ~ is used for includible elements that want to parse content for each line.
        - Used it for elements! Not items!
          So:

            'test'  : ['test', '%', '!expression'] -> test x < 10  valid

                                                      test          also valid
                                                      x < 10

            '~test' : ['test', '%', '!expression'] -> test         not valid
                                                       x < 10

                                                       test x < 10  valid
                                                       test x < 10
        - # means the element must be a one-liner. Can't be used together with ~.
        - & is special. It is used for dependency. Only use it for block!
            - The format, unlike other characters, are: current_element&previous_element
            - Use it in items! block = []
    WTF IS VALUE:
    - Now, there is value. Which as I've demonstrated before, is a list of string.
        - The list of string represents the syntax.
    - There are special characters, which can be backslashed.
        - % means whitespaces.
        - ! means a variable.
        - Between item of the list, whitespaces will be put.
            - Except before the first item and after the last item.
        - ? means to include a key from the same directive.
            - You can't use other special characters btw if you use ?
            - YOu can't self reference.
        - / is used in conjunction with $.
            - It basically means: If the variable is empty, use the word after the slash.

        Example: 'test' : ['test%abc', '!item%abc', 'in', '!a_list/default_list', '?a_key']

    - There are characters you can't type (not even backslash can help):
        -  [
        -  ]
        -  [/
        -  /]
        - Attempting to use these characters will result in an error.

    Copyright (c) 2024, All right reserved.
    This software is licensed in MIT License.

"""

TokenType = dict['str',  dict['str', list['str']]]

DIRECTIVES: TokenType = {
    'each' : {
        'attr'                : ['!item', 'in', '!iterable'],
        'content'             : ['!HTML_Element'],
        'short'               : ['!item', 'in', '!iterable', '::', '!HTML_Element']
    },
    'if' :   {
        'opening'             : ['if'],
        'content'             : ['if', 'elif', 'elif-content', 'else', 'else-content'],
        'if'                  : ['if', '!expression'],
        ':if-content'         : ['!HTML_Element'],
        '^*elif'              : ['elif', '!expression'],
        '*:elif-content&elif' : ['!HTML_Element'],
        '*else'               : ['else'],
        '*:else-content&else' : ['!HTML_Element'],
        'closing'             : ['if'],

        'short'               : ['if', '::', '?short-content'],
        'short-content&short' : ['!expression/else', ':', '!HTML_Element']
    },
    'match' : {
        'opening'             : ['match', '%', '!variable'],
        ''
        '^*:case'             : ['case', '%', '!compare/default'],
        'case-content&case'   : ['!HTML_Element'],
        'closing'             : ['match'],

        'short'               : ['match',  '%', '!variable', '::', '?short-content'],
        'short-content&short' : [ '!compare/default', ':', '!HTML_Element']
    },
    'script' : {
        'opening'             : ['script', '?attribute'],
        'attribute'           : ['indentation', '=', '!indentation_number'],
        '~content'            : ['!indentation', '!script'],
        'closing'             : ['script'],

        'short'               : ['script', '::', '!script']
    }
}

