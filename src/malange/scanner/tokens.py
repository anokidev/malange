"""

    malange.scanner.tokens

    This is the tokens.

"""

class Token:
    """A token class."""

    def __init__(self, group: str, char: str, span: tuple[int, int],
                 keywords: dict[str, str]):

        self.char:     str              = char
        self.span:     tuple[int, int]  = span
        self.group:    str              = group
        self.keywords: dict[str, str]   = keywords

    def __str__(self) -> str:
        try:
            return f"{self.group}::{self.keywords[self.char]}{self.span}"
        except KeyError:
            raise

class Tokenize:
    """Tokenize a character."""

    def  __init__(self, group: str, keywords: dict[str, str]) -> None:
        # Used for HTML and Malange directive name list.
        self.group:    str            = group
        self.keywords: dict[str, str] = keywords

    def __call__(self, char: str, span: tuple[int, int]) -> Token:
        return Token(self.group, char, span, self.keywords)
