"""

    malange.scanner.lexer

    This is the lexer file, that does these things:
    - Scan the .mala file.
    - Split lines.
    - Tokenize each line.
    - Return a list of tokens.

"""

import re
from typing import Match, Iterable
from .tokens import Tokenize

class ScannerComponent:
    """Scanner Main Class."""

    def __init__(self, text: str, direc_name: dict[str, str]):

        self.raw_text:     str            = text
        self.clean_text:   dict[int, str] = {}
        self.direc_name:   dict[str, str] = direc_name

        self.direc_tokenize   = Tokenize("DIREC_KEYWORD", self.direc_name)
        self.delimit_tokenize = Tokenize("MALANGE_DELIMITER", {
            "["  : "OPENING_BRACKET",
            "[/" : "OPENING_BRACKET_SLASH",
            "]"  : "CLOSING_BRACKET",
            "/]" : "CLOSING_BRACKET_SLASH",
            "<"  : "OPENING_TAG",
            "</" : "OPENING_TAG_SLASH",
            ">"  : "CLOSING_TAG",
            "/>" : "CLOSING_TAG_SLASH"
        })

        for index, line in enumerate(text.splitlines()):
            if line:
                self.clean_text[index] = line
                token_list = self.tokenize_line(line)

    def tokenize_line(self, line: str) -> dict[]:
        """Tokenize each line."""

        pipe:                  str = "|"

        opening_bracket:       str = r"(\[)"     + pipe
        closing_bracket:       str = r"(\])"     + pipe
        opening_bracket_slash: str = r"(\[\/)"   + pipe
        closing_bracket_slash: str = r"(\/\])"   + pipe

        opening_tag:           str = r"(<)"      + pipe
        closing_tag:           str = r"(>)"
        opening_tag_slash:     str = r"(<\/)"    + pipe
        closing_tag_slash:     str = r"(\/>)"    + pipe

        combined_pattern: str = (
            opening_bracket_slash + closing_bracket_slash +
            opening_bracket + closing_bracket +
            opening_tag_slash + closing_tag_slash +
            opening_tag + closing_tag
        )

        self.handle_regex(re.finditer(combined_pattern, line))

    def handle_regex(self, result: Iterable[Match[str]]):
        """Handle the regex."""

        for index, item in enumerate(result):

            data: str             = item.group()
            span: tuple[int, int] = item.span()

            tokenize(data, span)
