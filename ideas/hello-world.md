# Hello, world!

This is a simple hello world program.

```malange

[script]
    print("Hello, world!")
[/script]

<h1>Hello, world!</h1>

```

Create a malange file with the `.mala` extension.
