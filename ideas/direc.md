# Directives

This is the idea regarding directives.

## Format

There are two types of directives: Long and short.

```malange
# long directives
[direc args]
...
(subdirec)
...
[/subdirec]

# short directives

[direc args :: content /]
```

## Defining a Directive

There are two class you need to define:
- One template class.
- One execution class.

## Template Class

Here in this class you will define how your directive
will be structured. Here is how it works:

```python

```

